<?php

namespace KDA\KeyPage;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{


    use \KDA\Laravel\Traits\HasHelper;

    use \KDA\Laravel\Traits\HasConfig;

    
    protected $configs = [
        'kda/keypages.php'  => 'kda.keypages'
    ];

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    
}
