<?php


if (!function_exists('get_key_page')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function get_key_page($key)
    {
        $config = config('kda.keypages');
        return $config[$key] ?? NULL;
    }
}
